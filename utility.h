#ifndef __PNG2LAYOUT_UTILITY
#define __PNG2LAYOUT_UTILITY

#include <limits.h>
#include <string>

template <class T>
T max( T a, T b ) { return (a>b) ? a : b; };
template <class T>
T min( T a, T b ) { return (a<b) ? a : b; };

int parse_long( long* var, char const* text, long min, long max=LONG_MAX );
int parse_short( short* var, char const* text, short min, short max=SHRT_MAX );
int parse_bool( bool* var, char const* text );

std::string convert_whitespace( char const* c_str );

#endif // __PNG2LAYOUT_UTILITY
