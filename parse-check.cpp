/*
** This code is distributed under a modified BSD license.  Please refer
** to the accompanying LICENSE.txt.
**
*************************************************************************
**
** Copyright (c) 2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
*/

#include "utility.h"
#include <errno.h>
#include <gtest/gtest.h>

TEST(parse_long,good_data) {
	long ret;

	EXPECT_FALSE( parse_long( &ret, "0", 0, 1000000 ) );
	EXPECT_EQ( ret, 0 );
	EXPECT_FALSE( parse_long( &ret, "12345", 0, 1000000 ) );
	EXPECT_EQ( ret, 12345 );
	EXPECT_FALSE( parse_long( &ret, "+12345", 0, 10000000 ) );
	EXPECT_EQ( ret, 12345 );
	EXPECT_FALSE( parse_long( &ret, "-12345", -1000000, 1000000 ) );
	EXPECT_EQ( ret, -12345 );
}

TEST(parse_long,good_data_spaces) {
	long ret;

	EXPECT_FALSE( parse_long( &ret, "  0", 0, 100 ) );
	EXPECT_FALSE( parse_long( &ret, "0  ", 0, 100 ) );
	EXPECT_FALSE( parse_long( &ret, "  0  ", 0, 100 ) );
}

TEST(parse_long,bad_data) {
	long ret;

	EXPECT_EQ( parse_long( &ret, "", 0, 1000 ), EINVAL );
	EXPECT_EQ( parse_long( &ret, "a", 0, 1000 ), EINVAL );
	EXPECT_EQ( parse_long( &ret, "12a", 0, 1000 ), EINVAL );
	EXPECT_EQ( parse_long( &ret, "a12", 0, 1000 ), EINVAL );
	EXPECT_EQ( parse_long( &ret, "12a12", 0, 1000 ), EINVAL );
}

TEST(parse_long,bad_sign) {
	long ret;

	EXPECT_EQ( parse_long( &ret, "+", 1, 100 ), EINVAL );
	EXPECT_EQ( parse_long( &ret, "-", 1, 100 ), EINVAL );
	EXPECT_EQ( parse_long( &ret,"+-12345", 1, 100 ), EINVAL );
	EXPECT_EQ( parse_long( &ret,"12345+", 1, 100 ), EINVAL );
}

TEST(parse_long,bad_range) {
	long ret;

	EXPECT_EQ( parse_long( &ret, "0", 1, 100 ), ERANGE );
	EXPECT_EQ( parse_long( &ret, "101", 1, 100 ), ERANGE );
	EXPECT_EQ( parse_long( &ret, "12345678901234567890", LONG_MIN, LONG_MAX ), ERANGE );
	EXPECT_EQ( errno, 0 );
	EXPECT_EQ( parse_long( &ret, "-12345678901234567890", LONG_MIN, LONG_MAX ), ERANGE );
	EXPECT_EQ( errno, 0 );
}

TEST(parse_bool,good_data) {
	bool ret;

	EXPECT_FALSE( parse_bool( &ret, "true" ) );
	EXPECT_FALSE( parse_bool( &ret, "t" ) );
	EXPECT_FALSE( parse_bool( &ret, "false" ) );
	EXPECT_FALSE( parse_bool( &ret, "f" ) );
	EXPECT_FALSE( parse_bool( &ret, "yes" ) );
	EXPECT_FALSE( parse_bool( &ret, "y" ) );
	EXPECT_FALSE( parse_bool( &ret, "no" ) );
	EXPECT_FALSE( parse_bool( &ret, "n" ) );
	EXPECT_FALSE( parse_bool( &ret, "1" ) );
	EXPECT_FALSE( parse_bool( &ret, "0" ) );
}

TEST(parse_bool,bad_data) {
	bool ret;

	EXPECT_EQ( parse_bool( &ret, "" ), EINVAL );
	EXPECT_EQ( parse_bool( &ret, " " ), EINVAL );
}

int main( int argc, char* argv[] ) {
	testing::InitGoogleTest( &argc, argv );
	return RUN_ALL_TESTS();
}

