/*
** This code is distributed under a modified BSD license.  Please refer
** to the accompanying LICENSE.txt.
**
*************************************************************************
**
** Copyright (c) 2006,2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This file contains the entire implementation of the command-line
** application 'png2layout'.  The purpose of the program is to convert an
** image, stored in a PNG file (Portable Network Graphic), to a layout
** for use with common EDA tools.
**
** Colour images are converted to grayscale prior to converson.
**
** Grayscale images, included the converted colour images, are converted
** to black and white images using a threshold value.
**
** Using the black and white image, a CalTech Interchange Format (CIF)
** file containing layout information is created.
**
*/

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <png.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <vector>
#include <boost/polygon/polygon.hpp>
#include "hfile.h"
#include "utility.h"

extern bool read_image( boost::polygon::polygon_set_data<int>& out, char const* in_filename );


//////////////////////////////////////////////////////
// Forward function declarations
static void print_usage( std::ostream& );
static void print_version( std::ostream& );
static bool process( char const* out_filename, char const* in_filename );

//////////////////////////////////////////////////////
// Global variables
std::string	layer_name( "UNKNOWN" );
std::string	cell_name( "" );
std::string	out_filename( "" );
short		pixel_erode = 0;
long		pixel_size = 1;
short		threshold = 0x80;
bool		verbose = false;

//////////////////////////////////////////////////////
// Program entry
int main( int argc, char* argv[] )
{
	// check that we have at least one parameter, this is in 
	// addition to the program name
	if ( argc<2 ) 
	{
		print_usage( std::cerr );
		return EXIT_FAILURE;
	}
	
	// if we have exactly two arguments, we might be requesting
	// either help or version information 
	if ( argc==2 && argv[1][0]=='-' )
	{
		// there is one command-line argument
		// that argument is a command switch
		
		// first, ignore first dash if there are two
		if ( argv[1][1]=='-' ) ++argv[1];
		assert( argv[1][0]=='-' );
		
		// is it a request for help?
		if ( tolower( argv[1][1] ) == 'h' )
		{
			print_usage( std::cout );
			return EXIT_SUCCESS;
		}
		
		// is it a request for version information?
		if ( tolower( argv[1][1] ) == 'v' )
		{
			print_version( std::cout );
			return EXIT_SUCCESS;
		}
		
	}
	
	// skip the program name
	++argv;
	// get command line options
	while ( *argv ) 
	{
		// to simplify this loop
		// store the string currently being processed
		char const* str = *argv;

		// check if the current string contains
		// an option to be processed
		if ( str[0] == '-' ) 
		{
			// only the first character after the dash
			// is significant in determining what option
			// is being set
			switch ( tolower(str[1]) )
			{
			case 'e':
				++argv;
				if ( parse_short( &pixel_erode, *argv, -3, 3 ) ) {
					std::cerr << "ERROR: The pixel erode/dilate count must be an integer between -3 and 3." << std::endl;
					return EXIT_FAILURE;
				}
				break;
				
			case 'l':
				++argv;
				layer_name = convert_whitespace( *argv );
				if ( layer_name.empty() ) layer_name = "UNKNOWN";
				break;

			case 'n':
				++argv;
				cell_name = convert_whitespace( *argv );
				break;

			case 'o':
				++argv;
				if ( !out_filename.empty() )
					std::cerr << "NOTE: Overriding previously specified output filename." << std::endl;
				out_filename = *argv;
				break;
				
			case 's':
				++argv;
				if ( parse_long( &pixel_size, *argv, 1 ) ) {
					std::cerr << "ERROR: The pixel size must be an integer greater than zero." << std::endl;
					return EXIT_FAILURE;
				}
				// Because of definition of rectangles in CIF, widths
				// or heights with an odd number of units can lead
				// to import failures or incorrect results
				if ( pixel_size % 2 ) {
					std::cerr << "WARNING:  Some software may fail to properly import a CIF file if the pixel size is an odd number." << std::endl;
					// just a warning, so no exit
				}
				break;
				
			case 't':
				++argv;
				if ( parse_short( &threshold, *argv, 1, 0xfe ) ) {
					std::cerr << "ERROR: The pixel threshold must an integer between 1 and 254." << std::endl;
					return EXIT_FAILURE;
				}
				break;

			case 'v':
				++argv;
				if ( parse_bool( &verbose, *argv ) ) {
					std::cerr << "ERROR: The flag for verbose behaviour must be a boolean value." << std::endl;
					return EXIT_FAILURE;
				}
				break;

			default:
				std::cerr << "Unknown argument for application.\n\n";
				print_usage( std::cerr );
				return EXIT_FAILURE;
			}
		}
		else 
		{
			// We need to know the output filename
			// If no filename has been specified, create one
			// based on input filename
			if ( out_filename.empty() )
				out_filename = std::string(*argv) + ".cif";

			// we are going to process a file
			bool success = process( out_filename.c_str(), *argv );
			if ( !success ) return EXIT_FAILURE;

			// Clear out the output filename
			// Subsequent files will need new filenames
			out_filename.clear();
		}

		// move to next argument
		++argv;
	}

	// processing is complete
	return EXIT_SUCCESS;
}

static void print_usage( std::ostream& out )
{
	out << "Usage for png2layout:\n"
		"\tpng2layout [options] filename [ [options] filename ...]\n"
		"\tpng2layout -h\tRequest help.  Print this message.\n"
		"\tpng2layout -v\tRequest version information.\n"
		"\nOptions:\n"
		"\t-e [integer]\tset number of erode/dilate cycles\n"
		"\t-l [name]\tmake layer name.\n"
		"\t-n [name]\tcell name.\n"
		"\t-o [name]\toutput filename.\n"
		"\t-s [integer]\tpixel size in CIF DB units.\n"
		"\t-t [integer]\tthreshold for greyscale and colour conversion.\n"
		"\t-v [bool]\tflag to turn on verbose behaviour.\n";
}

static void print_version( std::ostream& out )
{
	out << PACKAGE_NAME ":  " PACKAGE_VERSION "\n"
		"PNG Library:  " PNG_HEADER_VERSION_STRING;
}

static std::string get_filename_title( char const* filename )
{
	assert( filename );

	// Skip any path information prior to file name
	char const* tmp;
	while( (tmp = strchr( filename, '/' ))!=0 ) filename = tmp+1;
	while( (tmp = strchr( filename, '\\' ))!=0 ) filename = tmp+1;
	
	// Only keep information before first period
	char const* c = strchr( filename, '.' );
	if ( c ) {
		return std::string( filename, 0, (c-filename) );
	}
	else {
		return filename;
	}
}

static int sign_of_determinant( int a00, int a01, int a10, int a11 )
{
	long det = long(a00)*a11 - long(a01)*a10;
	return (det>0) - (det<0);
}

static int sign_of_determinant( int a00, int a01, int a02,
	int a10, int a11, int a12,
	int a20, int a21, int a22 )
{
	long det = long(a00)*a11*a22 + long(a01)*a12*a20 + long(a02)*a10*a21
		- long(a00)*a12*a21 - long(a01)*a10*a22 + long(a02)*a11*a20;
	return (det>0) - (det<0);
}

typedef std::vector< boost::polygon::point_data<int> > polygon_data;

static bool is_collinear( polygon_data::iterator a, polygon_data::iterator b, polygon_data::iterator c )
{
	return sign_of_determinant(b->x()-a->x(), b->y()-a->y(), c->x()-a->x(), c->y()-a->y() ) == 0;
}

std::ostream& operator<<(std::ostream& out, boost::polygon::point_data<int> const& pt )
{
	out << pt.x() << ',' << pt.y();
	return out;
}

static void eliminate_collinear_points( polygon_data& pgn )
{
	if ( pgn.front() == pgn.back() )
		pgn.pop_back();

	polygon_data::iterator a = pgn.begin();
	polygon_data::iterator b = a; ++b;
	polygon_data::iterator c = b; ++c;

	while ( a != pgn.end() ) {
		if ( is_collinear( a, b, c ) ) {
			pgn.erase(b);
			b = a; ++b;
			if ( b==pgn.end() ) b = pgn.begin();
			c = b; ++c;
			if ( c==pgn.end() ) c = pgn.begin();
		}
		else {
			++a;
			b = c;
			++c;
			if ( c==pgn.end() ) c = pgn.begin();
		}
	}
}

static void write_points_simple( FILE* fout, polygon_data const& pgn )
{
	for (polygon_data::const_iterator j = pgn.begin(); j != pgn.end(); ++j ) {
		fprintf( fout, " %d,%d", j->x(), j->y() );
	}
}

static double distance( polygon_data const& pgn, size_t a, size_t m, size_t b )
{
	double ax = pgn[a].x(), bx = pgn[b].x(), cx = pgn[m].x();
	double ay = pgn[a].y(), by = pgn[b].y(), cy = pgn[m].y();

	double r_num = (cx-ax)*(bx-ax) + (cy-ay)*(by-ay);
	double r_den = (bx-ax)*(bx-ax) + (by-ay)*(by-ay);
	double r = r_num / r_den;
	if ( r>=0 && r<=1 ) {
		double px = ax + r* (bx-ax);
		double py = ay + r* (by-ay);
		return sqrt( (cx-px)*(cx-px)+(cy-py)*(cy-py) );
	}
	else {
		double dist1 = (cx-ax)*(cx-ax) + (cy-ay)*(cy-ay);
		double dist2 = (cx-bx)*(cx-bx) + (cy-by)*(cy-by);
		if ( dist1<dist2 )
			return sqrt(dist1);
		return sqrt(dist2);
	}

}

static void test()
{
	polygon_data c;
	c.push_back( boost::polygon::point_data<int>( 0, 0 ) );
	c.push_back( boost::polygon::point_data<int>( 90, 10 ) );
	c.push_back( boost::polygon::point_data<int>( 100, 0 ) );

	double d = distance( c, 0, 1, 2 );
	std::cout << "test " << d << '\n';
}

static void write_points( FILE* fout, polygon_data const& pgn, size_t a, size_t b )
{
	std::cout << "write_points "<< a << ' ' << b << '\n';

	//Ramer–Douglas–Peucker algorithm

	size_t m = (a+1) % pgn.size();
	if ( m==b ) {
		fprintf( fout, " %d,%d", pgn[a].x(), pgn[a].y() );
		return;
	}

	double dmax = distance( pgn, a, m, b );
	for ( size_t lp = (m+1) % pgn.size(); lp != b; lp = (lp+1) % pgn.size() ) {
		double tmp = distance( pgn, a, lp, b );
		if ( tmp > dmax ) {
			dmax = tmp;
			m = lp;
		}
		assert( dmax >= 0 );
	}

	std::cout << "\t" << dmax << ' ' << pixel_size << '\n';
	if ( dmax > 3.0*pixel_size ) {
		write_points( fout, pgn, a, m );		
		write_points( fout, pgn, m, b );
	}
	else {
		fprintf( fout, " %d,%d", pgn[a].x(), pgn[a].y() );
	}		
}

#define UPDATE(name,f,fs,s,ss) if (pgn[lp]. f () fs pgn[name]. f () ) { \
	name = lp; \
} else if ( pgn[lp]. f () == pgn[name]. f () && pgn[lp]. s () ss pgn[name]. s () ) { \
	name = lp; \
}

static void write_points( FILE* fout, polygon_data const& pgn )
{
	std::cout << "write_points\n";
	assert( pgn.front() != pgn.back() );

	size_t bl = 0, br=0, rb=0, rt=0, tr=0, tl=0, lt=0, lb=0;
	for ( size_t lp = 1; lp<pgn.size(); ++lp ) {
		// check for bottom left
		UPDATE( bl, y, <, x, < );
		// check for bottom right
		UPDATE( br, y, <, x, > );
		// check for right bottom
		UPDATE( rb, x, >, y, < );
		// check for right top
		UPDATE( rt, x, >, y, > );
		// check for top right
		UPDATE( tr, y, >, x, > );
		// check for top left
		UPDATE( tl, y, >, x, < );
		// check for left top
		UPDATE( lt, x, <, y, > );
		// check for left bottom
		UPDATE( lb, x, <, y, < );
	}

	if ( bl!=br ) write_points( fout, pgn, bl, br );
	if ( br!=rb ) write_points( fout, pgn, br, rb );
	if ( rb!=tr ) write_points( fout, pgn, rb, rt );
	if ( rt!=tr ) write_points( fout, pgn, rt, tr );
	if ( tr!=tl ) write_points( fout, pgn, tr, tl );
	if ( tl!=lt ) write_points( fout, pgn, tl, lt );
	if ( lt!=lb ) write_points( fout, pgn, lt, lb );
	if ( lb!=bl ) write_points( fout, pgn, lb, bl );
}

static bool process( char const* out_filename, char const* in_filename )
{
	assert( out_filename && *out_filename );
	assert( in_filename && *in_filename );
	assert( !layer_name.empty() );

	// First step, read in the PNG image
	boost::polygon::polygon_set_data<int> image;
	int success = read_image( image, in_filename );
	if ( !success ) return false;

	// Get trapezoids
	std::vector< boost::polygon::polygon_data<int> > shapes;
	image.get( shapes );
	
	// Third step, write out the layout file
	hFILE fout;
	fout.open( out_filename, "wt" );
	if ( !fout ) 
	{
		assert( errno );
		std::cerr << "Can not open the output file.\n"
			"\t" << out_filename << "\n"
			"\t" << strerror(errno) << std::endl;
		return false;
	}
	
	time_t now = time(0);
	fprintf( fout, "(CIF file written on %s by " PACKAGE_NAME " " PACKAGE_VERSION ");\n", ctime(&now) );
	fprintf( fout, "DS 1 1 1;\n" );
	fprintf( fout, "9 %s;\n", cell_name.empty() ? get_filename_title(in_filename).c_str() : cell_name.c_str() );
	fprintf( fout, "L %s;\n", layer_name.c_str() );

	// The geometry
	for ( size_t lp=0; lp<shapes.size(); ++lp ) {
		if ( shapes[lp].coords_.front() == shapes[lp].coords_.back() )
			shapes[lp].coords_.pop_back();

		fprintf( fout, "P" );
		write_points( fout, shapes[lp].coords_ );
		fprintf( fout, ";\n" );
	}
	// The footer
	fprintf( fout, "DF;\nC 1;\nE\n" );
	// Check for any errors during writing
	if ( fout.get_error() ) {
		std::cerr << "Could not write contents to the output file.\n"
			"\t" << out_filename << "\n"
			"\t" << strerror(errno) << std::endl;
		return false;
	}
	
	// That is all
	return true;
}

