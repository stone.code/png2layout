/*
** This code is distributed under a modified BSD license.  Please refer
** to the accompanying LICENSE.txt.
**
*************************************************************************
**
** Copyright (c) 2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This module contains minor support routines needed by png2layout.
** Mostly, these routines are used to verify and correct command-line
** arguments.
*/

#include "utility.h"
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int parse_long( long* var, char const* text, long min, long max )
{
	assert( var );
	assert( text );
	if ( !*text ) return EINVAL;

	char* endptr;
	int old_errno = errno;
	long ret = strtol( text, &endptr, 10 );
	if ( errno ) {
		int ret_errno = errno;
		errno = old_errno;
		return ret_errno;
	};
	errno = old_errno;

	while ( isspace( *endptr ) ) ++endptr;
	if ( *endptr ) return EINVAL;

	/* check for range */
	if ( ret < min ) return ERANGE;
	if ( ret > max ) return ERANGE;

	/* return value */
	*var = ret;
	return 0;
}

int parse_short( short* var, char const* text, short min, short max )
{
	long lvar;
	int ret = parse_long( &lvar, text, min, max );
	if ( ret ) return ret;
	assert( lvar >= min && lvar <= max );
	*var = lvar;
	return 0;
}

#ifndef HAVE_STRICMP
#define stricmp strcmp
#endif

int parse_bool( bool* var, char const* text )
{
	assert( var );
	assert( text );
	if ( !*text ) return EINVAL;

	/* Check for single character flags */
	if ( text[1]=='\0' ) switch ( text[0] ) {
		case 'f': case 'F':
		case 'n': case 'N':
		case '0':
			*var = false;
			return 0;

		case 't': case 'T':
		case 'y': case 'Y':
		case '1':
			*var = true;
			return 0;

		default:
			return EINVAL;
	}

	if ( stricmp( text, "false" )==0 || stricmp( text, "no" )==0 ) {
		*var = false;
		return 0;
	}
	if ( stricmp( text, "true" )==0 || stricmp( text, "yes" )==0 ) {
		*var = true;
		return 0;
	}

	return EINVAL;
}

#undef stricmp

