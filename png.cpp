/*
** This code is distributed under a modified BSD license.  Please refer
** to the accompanying LICENSE.txt.
**
*************************************************************************
**
** Copyright (c) 2006,2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
*/

#include <assert.h>
#include <errno.h>
#include <fstream>
#include <png.h>
#include <iostream>
#include <boost/polygon/polygon.hpp>
#include "hfile.h"

extern bool	verbose;
extern long	pixel_size;
extern short	threshold;

typedef boost::polygon::rectangle_data<int> rect;

static int read_image( png_structp& png_ptr, png_infop& info_ptr, png_infop& end_info, char const* filename )
{
	assert( !png_ptr );
	assert( !info_ptr );
	assert( !end_info );
	assert( filename );

	// Open the requested file and check that it is a PNG
	hFILE fp;
	fp.open( filename, "rb" );
	if (!fp) {
		assert( errno );
		std::cerr << "Can not open the input file.\n"
			"\t" << filename << "\n"
			"\t" << strerror(errno) << std::endl;
        	return false;
	}
	png_byte buffer[8];
	fread( buffer, 1, sizeof(buffer), fp);
	if ( png_sig_cmp( buffer, 0, sizeof(buffer) ) ) {
    		std::cerr << "It does not appear that the file specified is a PNG image." << std::endl;
    		return false;
	}
	
	// Read in all of the image data
	png_ptr = png_create_read_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
	if ( !png_ptr ) {
		std::cerr << "Out of memory.\n";
		return false;
	}
	info_ptr = png_create_info_struct( png_ptr );
	if ( !info_ptr ) {
		png_destroy_read_struct( &png_ptr, NULL, NULL );
		std::cerr << "Out of memory.\n";
		return false;
	}
	end_info = png_create_info_struct( png_ptr );
	if ( !end_info ) {
		png_destroy_read_struct( &png_ptr, &info_ptr, NULL );
		std::cerr << "Out of memory.\n";
		return false;
	}
	
	if ( setjmp( png_jmpbuf(png_ptr) ) ) {
		png_destroy_read_struct( &png_ptr, &info_ptr, &end_info );
		std::cerr << "Out of memory.\n";
		return false;
	}
	
	png_init_io( png_ptr, fp );
	png_set_sig_bytes( png_ptr, sizeof(buffer) );
	png_read_png( png_ptr, info_ptr, PNG_TRANSFORM_PACKING | PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_STRIP_ALPHA | PNG_TRANSFORM_EXPAND, NULL );
	assert( png_get_bit_depth( png_ptr, info_ptr)==8 );
	assert( PNG_COLOR_TYPE_RGB==png_get_color_type( png_ptr, info_ptr) || PNG_COLOR_TYPE_GRAY==png_get_color_type( png_ptr, info_ptr) );
	
	// Print out image statistics
	png_uint_32 rows = png_get_image_height( png_ptr, info_ptr );
	png_uint_32 cols = png_get_image_width( png_ptr, info_ptr );
	bool interlaced = ( PNG_INTERLACE_NONE==png_get_interlace_type( png_ptr, info_ptr ) );
	bool colour = ( PNG_COLOR_TYPE_RGB==png_get_color_type( png_ptr, info_ptr ) );
	if ( verbose ) {
		std::cout << "Geometry: " << cols << 'x' << rows << "  (" << (colour ? "Colour" : "Gray") << ")\n";
		std::cout << "Interlaced: " << (interlaced ? "Yes" : "No" ) << std::endl;
	}

	// If necessary, convert rgb data to grayscale data
	// Pack data into start of scanlines used originally for rgb data
	if ( colour ) {
		for (png_uint_32 row=0; row<rows; ++row ) {
			// get the row data
			png_bytep rowdata = png_get_rows( png_ptr, info_ptr )[row];
			// inplace, convert to grayscale data
			for ( png_uint_32 col=0; col<cols; ++col ) {
				long red = rowdata[ col*3 + 0 ];
				long green = rowdata[ col*3 + 1 ];
				long blue = rowdata[ col*3 + 2 ];
				long val = red * 30 / 100 + green * 59 / 100 + blue * 11 / 100;
				assert( val>=0 && val<0x100 );
				rowdata[col] = val;
			}
		}
	}
	
	// and we are done
	return true;
}

static void process_row( std::vector<rect>& rectangles, png_uint_32 row, png_bytep rowdata, png_uint_32 cols )
{
	assert( rowdata );
	assert( cols > 0 );

	png_uint_32 col = 0;
	
	do {
		if ( rowdata[col] <= threshold ) {
			png_uint_32 endcol = col + 1;
			while ( endcol<cols && rowdata[endcol] <= threshold ) ++endcol;
			boost::polygon::rectangle_data<int> rect( col * pixel_size,
				-row * pixel_size, endcol*pixel_size, (-row-1)*pixel_size );
			rectangles.push_back(rect);
			// move forward
			col = endcol+1;
		}
		else {
			++col;
		}
	} while ( col < cols );
}

bool read_image( boost::polygon::polygon_set_data<int>& out, char const* in_filename )
{
	assert( in_filename && *in_filename );

	png_structp png_ptr = 0;
	png_infop info_ptr = 0, end_info = 0;

	// Read in the PNG image
	int success = read_image( png_ptr, info_ptr, end_info, in_filename );
	if ( !success ) return false;
	
	// Convert to a boost polygon
	std::vector<rect> rectangles;
	png_uint_32 const rows = png_get_image_height( png_ptr, info_ptr );
	png_uint_32 const cols = png_get_image_width( png_ptr, info_ptr );
	for ( png_uint_32 row=0; row<rows; ++row ) {
		png_bytep rowdata = png_get_rows( png_ptr, info_ptr )[row];
		process_row( rectangles, row, rowdata, cols );
	}
	
	// That is all
	out.insert( rectangles.begin(), rectangles.end() );
	return true;
}

