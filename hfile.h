#ifndef __PNG2LAYOUT_HFILE
#define __PNG2LAYOUT_HFILE

// This class is a very thin wrapper around around the STD C standard IO
// routines.  The class only provides function for open, close, and to access
// any error codes.
//
// For other operations, use the routines from <stdio.h>

#include <stdio.h>

class hFILE
{
	public:
		hFILE() { fp=0; };
		~hFILE() { if (fp) fclose(fp); };
		void open( char const* filename, char const* mode )
		{ assert(!fp); FILE* newfp = fopen( filename,mode); fp=newfp; };
		void close()
		{ if (fp) fclose(fp); fp=0; };
		int get_error() 
		{ assert(fp); return ferror(fp); };
		
		operator FILE*() const { return fp; };
		
	private:
		FILE* fp;
};


#endif // __PNG2LAYOUT_HFILE
