/*
** This code is distributed under a modified BSD license.  Please refer
** to the accompanying LICENSE.txt.
**
*************************************************************************
**
** Copyright (c) 2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
*/

#include "utility.h"
#include <gtest/gtest.h>

TEST(convert_whitespace,test) {
	EXPECT_EQ( convert_whitespace(""), "" );
	EXPECT_EQ( convert_whitespace("asdf"), "asdf" );
	EXPECT_EQ( convert_whitespace("as df"), "as_df" );
	EXPECT_EQ( convert_whitespace("asdf "), "asdf_" );
	EXPECT_EQ( convert_whitespace(" asdf"), "_asdf" );
}

int main( int argc, char* argv[] ) {
	testing::InitGoogleTest( &argc, argv );
	return RUN_ALL_TESTS();
}

