/*
** This code is distributed under a modified BSD license.  Please refer
** to the accompanying LICENSE.txt.
**
*************************************************************************
**
** Copyright (c) 2006,2007 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This module contains minor support routines needed by png2layout.
** Mostly, these routines are used to verify and correct command-line
** arguments.
*/

#include "utility.h"
#include <assert.h>
#include <ctype.h>

std::string convert_whitespace( char const* c_str )
{
	assert( c_str );

	// create the return string
	std::string ret( c_str );

	// convert all whitespace
	size_t lp;
	for ( lp=0; lp<ret.size(); ++lp ) {
		if ( isspace( ret[lp] ) ) ret[lp] = '_';
	}
	
	// return result
	return ret;
}

